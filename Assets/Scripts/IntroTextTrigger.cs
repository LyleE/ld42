﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IntroTextTrigger : MonoBehaviour {

	private const float TWEEN_TIME = 1f;

	public TextMeshPro Text;

	public float Delay = 0f;

	private float _triggerTime;

	void Awake()
	{
		Text.alpha = 0f;
	}

	void OnTriggerEnter(Collider other)
	{
		// Debug.Log(other.gameObject, other.gameObject);
		// Text.CrossFadeAlpha(1f, 1f, true);

		_triggerTime = Time.time + Delay;
	}

	void Update()
	{
		if(_triggerTime == 0f)
		{
			return;
		}

		var delta = Time.time - _triggerTime;

		delta /= TWEEN_TIME;
		delta = Mathf.Clamp(delta, 0f, TWEEN_TIME);

		var alpha = Mathf.Lerp(0f, 1f, delta);
		var c = Text.color;
		c.a = alpha;
		Text.color = c;

		if(delta == 1f)
		{
			_triggerTime = 0f;
		}
	}
}
