﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager> {

	public AudioSource BGMusic;
	public AudioSource WinSfx;
	public GameObject StepSfxRoot;
	public AudioSource SpawnAnti;
	public AudioSource EatAnti;
	public AudioSource TileInfected;

	private AudioSource[] _steppys;

	private Coroutine _bgCo;

	protected override void SingletonAwake()
	{
		GameEngine.Instance.OnInitLevel.AddListener(HandleInit);
		GameEngine.Instance.OnWinLevel.AddListener(HandleOnWin);
		GameEngine.Instance.OnStep.AddListener(HandleOnStep);
		GameEngine.Instance.OnSpawnAnti.AddListener(HandleOnSpawnAnti);
		GameEngine.Instance.OnGetAntidote.AddListener(HandleOnEatAnti);
		GameEngine.Instance.OnKillAnti.AddListener(HandleOnKillAnti);

		_steppys = StepSfxRoot.GetComponents<AudioSource>();
	}

	private void HandleInit()
	{
		if(_bgCo != null)
		{
			StopCoroutine(_bgCo);
		}
		WinSfx.Stop();
		_bgCo = StartCoroutine(FadeIn(BGMusic));
	}

    private void HandleOnWin()
    {
		if(_bgCo != null)
		{
			StopCoroutine(_bgCo);
		}
		_bgCo = StartCoroutine(FadeOut(BGMusic));
        WinSfx.Play();
		// StartCoroutine(AfterDelay(2.05f, () => {
		// 	WinSfx.clip = WinLoopClip;
		// 	WinSfx.loop = true;
		// 	WinSfx.Play();
		// }));
    }

	private void HandleOnStep()
    {
		foreach(var steppy in _steppys)
		{
			if(steppy.isPlaying)
			{
				continue;
			}

	        steppy.Play();
			return;
		}
    }

	private void HandleOnEatAnti()
    {
        EatAnti.Play();
    }

	private void HandleOnSpawnAnti()
    {
        SpawnAnti.Play();
    }

	private void HandleOnKillAnti()
    {
        TileInfected.Play();
    }

	private IEnumerator AfterDelay(float delay, Action callback)
	{
		yield return new WaitForSeconds(delay);

		callback.Invoke();
	}

	private IEnumerator FadeOut(AudioSource audio, float totalTime = 1f)
	{
		float time = 0f;
		var volume = 1f;
		while(time < totalTime)
		{
			time += Time.deltaTime;
			volume = Mathf.Clamp01((totalTime - time) / totalTime);
			audio.volume = volume;
			yield return new WaitForEndOfFrame();
		}
	}

		private IEnumerator FadeIn(AudioSource audio, float totalTime = 1f)
	{
		float time = 0f;
		var volume = 0f;
		while(time < totalTime)
		{
			time += Time.deltaTime;
			volume = Mathf.Clamp01((time) / totalTime);
			audio.volume = volume;
			yield return new WaitForEndOfFrame();
		}
	}
}
