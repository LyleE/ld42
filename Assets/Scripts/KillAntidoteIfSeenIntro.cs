﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAntidoteIfSeenIntro : MonoBehaviour {

	// Use this for initialization
	void Awake()
	{
		if(GameEngine.Instance.HasSeenIntro())
		{
			var a = GetComponent<Antidote>();
			a.Kill();
		}
	}	
}
