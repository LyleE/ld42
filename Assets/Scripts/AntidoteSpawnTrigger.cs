﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntidoteSpawnTrigger : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		GameEngine.Instance.SetCanSpawnAntidotes();
	}
}