﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class LevelText : MonoBehaviour {

	private TextMeshProUGUI _text;

	// Use this for initialization
	public void Init()
	{
		_text = GetComponent<TextMeshProUGUI>();
		GameEngine.Instance.OnInitLevel.AddListener(HandleOnInitLevel);
	}

    private void HandleOnInitLevel()
    {
        _text.text = "Level "+(LevelManager.Instance.CurrentLevel+1)+" of "+LevelManager.Instance.LevelCount+"\n"+
		"\""+GameEngine.Instance.LevelName+"\"";
    }
}
