﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour {

	public GameObject Model;
	public bool IsMoving;
	public float Speed = 1f;

	private Rigidbody _rb;
	

	void Start()
	{
		_rb = GetComponent<Rigidbody>();
	}

	public void StartMoving(Vector3 dir)
	{
		IsMoving = true;
		_rb.velocity = Speed * dir;
	}

	public void StopMoving()
	{
		IsMoving = false;
		_rb.velocity = Vector3.zero;
	}

	public void Die()
	{
		//TODO animate?
		StopMoving();
		// Model.SetActive(false);
	}
}
