﻿using UnityEngine;
using System.Collections;

public static class Extensions
{
    public static void Shuffle<T>(T[] arr)  
    {  
        int n = arr.Length;  
        while (n > 1) {  
            n--;  
            int k = Random.Range(0, n+1);  
            T value = arr[k];  
            arr[k] = arr[n];  
            arr[n] = value;  
        }
    }
}