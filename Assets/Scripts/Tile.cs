﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class Tile : MonoBehaviour
{
	public enum TileState
	{
		Tile,
		Void,
		Tail
	}

	public MeshRenderer TileModel;
	public MeshRenderer CrackModel;

	public Material PoisonMaterialPrefab;
	public Material InfectedMaterialPrefab;

	public TileState State;

	private Animator _anim;


[Header("Links")]
	public Tile North;
	public Tile East;
	public Tile South;
	public Tile West;
	// Use this for initialization
	void Awake()
	{
		_anim = GetComponent<Animator>();
		TileModel.material = Instantiate(TileModel.material);
		CrackModel.material = Instantiate(PoisonMaterialPrefab);

		name = "Tile ("+transform.position.x+","+transform.position.y+")";
		State = TileState.Tile;
		
		InitLinks();
		TileVariations(TileModel.transform);
	}

	void TileVariations(Transform tf)
	{
		tf.localScale = new Vector3(
			tf.localScale.x * GetRandomSign(),
			tf.localScale.y * GetRandomSign(),
			tf.localScale.z
		);

		tf.localRotation = Quaternion.Euler(Vector3.forward * 90 * Random.Range(0,4));
	}

	int GetRandomSign()
	{
		return Random.value < 0.5f? -1: 1;
	}

	// void Update()
	// {
	// 	SetState(State);
	// }

	void InitLinks()
	{
		North = LookForLink(Vector3.up);
		South = LookForLink(Vector3.down);
		East = LookForLink(Vector3.right);
		West = LookForLink(Vector3.left);
	}

	void UpdateLinks(bool connected)
	{
		Tile newValue = connected? this : null;
		if(North != null)
		{
			North.South = newValue;
		}

		if(South != null)
		{
			South.North = newValue;
		}

		if(East != null)
		{
			East.West = newValue;
		}

		if(West != null)
		{
			West.East = newValue;
		}
	}

	Tile LookForLink(Vector3 dir)
	{
		var layerMask = 1 << LayerMask.NameToLayer("Tiles");
		RaycastHit hit;
		if(Physics.Raycast(transform.position, dir, out hit, 1f, layerMask))
		{
			var tile = hit.transform.GetComponent<Tile>();
			if(tile != null)
			{
				return tile;
			}

			Debug.LogError("Hit something that looks like a tile but isn't?");
		}
		else
		{
			//Debug.Log(name+" missed looking "+dir, gameObject);
		}

		return null;
	}

	public void SetState(TileState state)
	{
		switch(state)
		{
			default:
				Debug.LogError("????");
			break;
			case TileState.Tile:
				GoToHealthy();
				if(State == TileState.Void)
				{
					UpdateLinks(true);
				}
			break;
			case TileState.Void:
				GoToInfected();
				UpdateLinks(false);
			break;
			case TileState.Tail:
				 GoToPoisoned();
				if(State == TileState.Void)
				{
					UpdateLinks(true);
				}
			break;
		}
		
		State = state;
	}

	void GoToPoisoned()
	{
		TriggerAnim("poison");
	}

	void GoToHealthy()
	{
		TriggerAnim("healthy");
	}

	void GoToInfected()
	{
		CrackModel.material = Instantiate(InfectedMaterialPrefab);
		TriggerAnim("infected");
	}

	private void TriggerAnim(string triggerName)
	{
		_anim.SetTrigger(triggerName);
	}

	public Tile GetTileInDirection(Vector3 dir)
	{
		if(dir == Vector3.up)
		{
			return North;
		}

		if(dir == Vector3.down)
		{
			return South;
		}

		if(dir == Vector3.left)
		{
			return West;
		}

		if(dir == Vector3.right)
		{
			return East;
		}

		if(dir == Vector3.zero)
		{
			return null;
		}

		Debug.LogError("Weird direction? "+dir);
		return null;
	}

	public static bool IsValidTile(Tile t)
	{
		return t != null && t.State == Tile.TileState.Tile;
	}
}
