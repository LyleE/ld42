﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameEngine : Singleton<GameEngine> {

	public class GameEngineEvent: UnityEvent {}
	public GameEngineEvent OnInitLevel = new GameEngineEvent();
	public GameEngineEvent OnGetAntidote = new GameEngineEvent();
	public GameEngineEvent OnWinLevel = new GameEngineEvent();
	public GameEngineEvent OnStep = new GameEngineEvent();
	public GameEngineEvent OnSpawnAnti = new GameEngineEvent();
	public GameEngineEvent OnKillAnti = new GameEngineEvent();
	public GameEngineEvent OnFailLevel = new GameEngineEvent();
	public GameEngineEvent OnGameComplete = new GameEngineEvent();
	

	public const bool CheckForDuplicateTiles = true;

	public string LevelName;

	[Header("Snake")]
	public Snake Snake;
	public Tile CurrentTile;
	public int TailLength = 5;
	//TODO: make this false when you ship so the intro is seen.
	[SerializeField]
	private bool _seenIntro = false;
	public Vector3 IntroSpawnPoint;

	private Queue<Tile> _tailQueue;
	
	public Vector3 CurrentDirection;
	public Vector3 NextDirection;

	public bool IsDead;

	private float _nextTileTime;

	[Header("Antidotes")]
	public Antidote AntidotePrefab;
	public int AntidotesCollected = 0;
	public int AntidotesNeededToWin = 10;
	[SerializeField]
	private bool CanSpawnAntidotes = true;
	private Tile _antidoteTile;
	private Antidote _spawnedAntidote;
	
	private VoidChecker _voidChecker;

	private List<Tile> _allValidTiles;

	private Coroutine _antiSpawnCoroutine;

#region Unity Shiz

	protected override void SingletonAwake()
	{
		//level init started by levelconfig
	}

	void Update()
	{
		if(IsDead)
		{
			return;
		}

		if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
		{
			SetDirection(Vector3.up);
		}
		else if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
		{
			SetDirection(Vector3.down);
		}
		else if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
		{
			SetDirection(Vector3.left);
		}
		else if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
		{
			SetDirection(Vector3.right);
		}

		if(Time.time >= _nextTileTime)
		{
			HitNewTile();
		}

		CheckForAntidoteDeath();
	}
#endregion
#region Init

	public bool HasSeenIntro()
	{
		return _seenIntro;
	}

	public void InitialiseLevel(LevelConfig config)
	{
		// Debug.Log("InitialiseLevel: "+config, config.gameObject);

		LevelName = config.LevelName;
		IsDead = false;
		Snake = config.Snake;
		AntidotesCollected = 0;
		AntidotesNeededToWin = config.AntidotesRequired;
		TailLength = config.InitialTailLength;
		CurrentDirection = NextDirection = Vector3.zero;

		_tailQueue = new Queue<Tile>();
		if(!_seenIntro)
		{
			_seenIntro = true;
			Snake.transform.position = IntroSpawnPoint;
		}
		InitTiles();
		if(CurrentTile == null)
		{
			Debug.LogError("No start tile???");
		}

		OnInitLevel.Invoke();

		if(CanSpawnAntidotes)
		{
			SpawnAntidoteDelayed();
		}
	}

	void InitTiles()
	{
		CurrentTile = FindTileAt(Snake.transform.position);

		var allTiles = Resources.FindObjectsOfTypeAll<Tile>();
		_allValidTiles = new List<Tile>();

		var seenTilePositions = new List<Vector3>();

		foreach(var tile in allTiles)
		{
			if(tile.State != Tile.TileState.Void)
			{
				_allValidTiles.Add(tile);
				if(CheckForDuplicateTiles)
				{
					if(seenTilePositions.Contains(tile.transform.position))
					{
						Debug.Log("Duplicate tile! "+tile.name, tile.gameObject);
					}
					else
					{
						seenTilePositions.Add(tile.transform.position);
					}
				}
			}
		}

		_tailQueue.Clear();
		AddTailTile(CurrentTile);
	}

	Tile FindTileAt(Vector3 pos)
	{
		var layerMask = 1 << LayerMask.NameToLayer("Tiles");
		RaycastHit hit;
		if(Physics.Raycast(pos - Vector3.forward, Vector3.forward, out hit, 1f, layerMask))
		{
			var tile = hit.transform.GetComponent<Tile>();
			if(tile != null)
			{
				return tile;
			}

			Debug.LogError("Hit something that looks like a tile but isn't? "+hit.transform.name, hit.transform.gameObject);
		}

		return null;
	}
#endregion
#region Snake Movement
	void SetDirection(Vector3 dir)
	{
		NextDirection = dir;
	}
	
	void HitNewTile()
	{
		if(IsDead)
		{
			return;
		}

		if(!Snake.IsMoving)
		{
			TurnSnake(NextDirection);
		}

		if(NextDirection == -CurrentDirection)
		{
			NextDirection = CurrentDirection;
		}


		var nextTile = CurrentTile.GetTileInDirection(NextDirection);
		if(nextTile == null)
		{
			NextDirection = CurrentDirection;
			nextTile = CurrentTile.GetTileInDirection(CurrentDirection);
		}

		if(nextTile == null)
		{
			// Debug.Log("Can't move in the expected direction!");
			if(Snake.IsMoving)
			{
				AddTailTile(CurrentTile);
				Snake.StopMoving();
				CheckForSuffication();
				Snake.transform.position = CurrentTile.transform.position;
			}
			return;
		}

		if(Snake.IsMoving)
		{
			AddTailTile(CurrentTile);
		}

		if(nextTile.State == Tile.TileState.Void)
		{
			NextDirection = CurrentDirection;
			Snake.StopMoving();
			CheckForSuffication();
			return;
		}

		if(IsDead)
		{
			return;
		}

		TurnSnake(NextDirection);

		SetCurrentTile(nextTile);
		CurrentDirection = NextDirection;
		Snake.StartMoving(CurrentDirection);
		
		SetNextTileTime();
	}

	void SetNextTileTime()
	{
		_nextTileTime = Time.time + 1f/Snake.Speed;
	}

	void TurnSnake(Vector3 dir)
	{
		if(dir != Vector3.zero)
		{
			Snake.transform.localRotation = Quaternion.LookRotation(Vector3.forward, dir);
		}
		Snake.transform.position = CurrentTile.transform.position;
	}
	#endregion
#region Tail Tracking
	
	void AddTailTile(Tile newTile)
	{
		if(_tailQueue.Contains(newTile))
		{
			OnEatingItsOwnTail();
			return;
		}

		OnStep.Invoke();

		newTile.SetState(Tile.TileState.Tail);
		_tailQueue.Enqueue(newTile);

		// Debug.Log(_allValidTiles.Contains(newTile)+" == true?");
		_allValidTiles.Remove(newTile);
		// Debug.Log(_allValidTiles.Contains(newTile)+" == false?");

		if(_tailQueue.Count > TailLength)
		{
			var oldTile = _tailQueue.Dequeue();
			oldTile.SetState(Tile.TileState.Tile);
			// Debug.Log(_allValidTiles.Contains(oldTile)+" == false?");
			_allValidTiles.Add(oldTile);
			// Debug.Log(_allValidTiles.Contains(oldTile)+" == true?");
		}

		DoVoidCheck(newTile);
	}
	
	void SetCurrentTile(Tile nextTile)
	{
		CurrentTile = nextTile;
	}

	void OnEatingItsOwnTail()
	{
		if(IsDead)
		{
			return;
		}

		if(_antiSpawnCoroutine != null)
		{
			StopCoroutine(_antiSpawnCoroutine);
			_antiSpawnCoroutine = null;
		}

		IsDead = true;
		Debug.Log(Time.frameCount+" DEAD");
		Snake.Die();
		VoidAllRemainingTiles();
		OnFailLevel.Invoke();

		//TODO replace with death anim
		LevelManager.Instance.ReloadCurrentLevel();
	}

	void CheckForSuffication()
	{
		if(IsDead)
		{
			return;
		}

		// Debug.Log(CurrentTile.name, CurrentTile.gameObject);

		if(
			Tile.IsValidTile(CurrentTile.North) ||
			Tile.IsValidTile(CurrentTile.South) ||
			Tile.IsValidTile(CurrentTile.East) ||
			Tile.IsValidTile(CurrentTile.West) )
		{
			return;
		}

		Debug.Log("Suffocated!");
		OnEatingItsOwnTail();
	}

	void VoidAllRemainingTiles()
	{
		foreach(var tile in _allValidTiles)
		{
			tile.SetState(Tile.TileState.Void);
			// _allNonvoidTiles.Remove(tile);
		}
	}

#endregion
#region Void Detection

	void DoVoidCheck(Tile t)
	{
		if(_voidChecker == null)
		{
			_voidChecker = new VoidChecker();
		}

		//Debug.Log(Time.frameCount+" running void check on "+t);

		_voidChecker.DoVoidCheckOn(t, _tailQueue.Peek());
		//_voidChecker.PrintResult();

		if(_voidChecker.FoundIslands.Count < 2)
		{
			return;
		}

		int biggestIslandSize = 0;
		List<int> biggestIslandIndexes = new List<int>();

		for(int i = 0; i < _voidChecker.FoundIslands.Count; i++ )
		{
			var island = _voidChecker.FoundIslands[i];
			
			if(island.Count < biggestIslandSize)
			{
				//smaller than the biggest -> move on
				continue;
			}

			if(island.Count > biggestIslandSize)
			{
				//new record for the biggest -> clear old lists and save this
				biggestIslandSize = island.Count;
				biggestIslandIndexes.Clear();
			}

			biggestIslandIndexes.Add(i);
		}

		var islandIndexToKeep = biggestIslandIndexes[0];

		for(int i = 0; i < _voidChecker.FoundIslands.Count; i++)
		{
			if(i != islandIndexToKeep)
			{
				foreach(var tile in _voidChecker.FoundIslands[i])
				{
					tile.SetState(Tile.TileState.Void);
					_allValidTiles.Remove(tile);
				}
			}
		}
	}
#endregion
#region Antidotes
	public void OnAntidoteCollected()
	{
		Debug.Log("Collected!");
		AntidotesCollected++;
		OnGetAntidote.Invoke();

		if(AntidotesCollected >= AntidotesNeededToWin)
		{
			WinLevel();
		}
		else if(CanSpawnAntidotes)
		{
			Snake.Speed *= 1.12f;
			TailLength++;
			SpawnAntidoteDelayed();
		}
	}

	public void DebugCanSpawnEnable()
	{
		CanSpawnAntidotes = true;
	}

	public void SetCanSpawnAntidotes()
	{
		if(CanSpawnAntidotes)
		{
			//already enabled my dude
			return;
		}
		CanSpawnAntidotes = true;
		SpawnAntidoteNow();
	}

	void SpawnAntidoteDelayed()
	{
		if(IsDead)
		{
			return;
		}
		
		// Debug.Log(Time.frameCount+" ANTI SPAWNED", gameObject);
		if(_antiSpawnCoroutine != null)
		{
			StopCoroutine(_antiSpawnCoroutine);
		}
		_antiSpawnCoroutine = StartCoroutine(_SpawnAntidoteDelayed());
	}

	IEnumerator _SpawnAntidoteDelayed()
	{
		var delay = Random.Range(0.5f,2f);

		yield return new WaitForSeconds(delay);

		SpawnAntidoteNow();
	}
	
	void SpawnAntidoteNow()
	{
		if(IsDead)
		{
			return;
		}

		_antidoteTile = GetSpawnPosition();
		_spawnedAntidote = Instantiate<Antidote>(AntidotePrefab, _antidoteTile.transform.position, Quaternion.identity);
		OnSpawnAnti.Invoke();
	}

	Tile GetSpawnPosition()
	{
		var randomTile = _allValidTiles[Random.Range(0, _allValidTiles.Count)];
		if(SceneManager.GetActiveScene().buildIndex == 0 && AntidotesCollected < 2)
		{
			//prevent the very first "real" antidote being spawned in front of the player -> force them to move to get it.
			while(Mathf.Abs(randomTile.transform.position.x) < 2)
			{
				// Debug.Log(randomTile.name+" no good! AGAIN!!!");
				randomTile = _allValidTiles[Random.Range(0, _allValidTiles.Count)];
			}
		}

		// Debug.Log(randomTile.name+" good.");
		return randomTile;
	}

	void CheckForAntidoteDeath()
	{
		if(_antidoteTile == null || _spawnedAntidote == null)
		{
			return;
		}

		if(_antidoteTile.State == Tile.TileState.Void)
		{
			_spawnedAntidote.Kill();
			_antidoteTile = null;
			_spawnedAntidote = null;
			SpawnAntidoteDelayed();
			OnKillAnti.Invoke();
		}
	}

	void WinLevel()
	{
		Debug.Log("Winrar");
		IsDead = true;
		Snake.StopMoving();
		OnWinLevel.Invoke();

		if(_antiSpawnCoroutine != null)
		{
			StopCoroutine(_antiSpawnCoroutine);
			_antiSpawnCoroutine = null;
		}

		//TODO replace with death anim
		LevelManager.Instance.LoadNextLevel();
	}

	public void TriggerTheEnd()
	{
		OnGameComplete.Invoke();
	}

#endregion
}
