﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WinWash : MonoBehaviour {

	public AnimationClip _FadeInAnim;
	public AnimationClip _FadeOutAnim;
	
	private Animation _anim;
	private bool _isFadedIn;

	public void Init()
	{
		_anim = GetComponent<Animation>();
		GameEngine.Instance.OnInitLevel.AddListener(HandleLevelInit);
		GameEngine.Instance.OnWinLevel.AddListener(HandleWinLevel);
	}

	private void HandleLevelInit()
	{
		if(_isFadedIn)
		{
			_anim.clip = _FadeOutAnim;
			_anim.Play();
			_isFadedIn = false;
		}
	}

    private void HandleWinLevel()
    {
		if(!_isFadedIn)
		{
			_anim.clip = _FadeInAnim;
			_anim.Play();
			_isFadedIn = true;
		}
    }
}
