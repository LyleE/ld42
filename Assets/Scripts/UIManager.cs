﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager> {

    public WinWash WinWash;
    public LoseText LoseText;
    public CollectionUI CollectionUI;
    public LevelText LevelText;

	protected override void SingletonAwake()
    {
        WinWash.Init();
        LoseText.Init();
        CollectionUI.Init();
        LevelText.Init();
    }
}
