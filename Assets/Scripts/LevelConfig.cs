﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConfig : MonoBehaviour
{
	public string LevelName;
	public Snake Snake;
	public int InitialTailLength = 10;
	public int AntidotesRequired = 10;
	
	void Start()
	{
		GameEngine.Instance.InitialiseLevel(this);
	}
}
