﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class LoseText : MonoBehaviour {

	private const float TWEEN_TIME = 1f;

	public float Delay = 0f;

	private float _triggerTime;
	TextMeshProUGUI _text;

	public void Init()
	{
		_text = GetComponent<TextMeshProUGUI>();
		_text.alpha = 0f;

		GameEngine.Instance.OnInitLevel.AddListener(HandleLevelInit);
		GameEngine.Instance.OnFailLevel.AddListener(HandleFailLevel);
		GameEngine.Instance.OnGameComplete.AddListener(HandleGameComplete);
	}

	private void HandleLevelInit()
	{
		_text.alpha = 0f;
		_triggerTime = 0f;
	}

    private void HandleFailLevel()
    {
		_triggerTime = Time.time + Delay;
	}

	private void HandleGameComplete()
	{
		_text.text = "Thank you for playing.";
	}

	void Update()
	{
		if(_triggerTime == 0f)
		{
			return;
		}

		var delta = Time.time - _triggerTime;

		delta /= TWEEN_TIME;
		delta = Mathf.Clamp(delta, 0f, TWEEN_TIME);

		var alpha = Mathf.Lerp(0f, 1f, delta);
		var c = _text.color;
		c.a = alpha;
		_text.color = c;

		if(delta == 1f)
		{
			_triggerTime = 0f;
		}
	}
}
