﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoidChecker {

	public List<List<Tile>> FoundIslands;

	private List<Tile> _seenTiles;
	private List<Tile> _currentIsland;
	private Queue<Tile> _frontierTiles;

	public void Clear()
	{
		FoundIslands = null;
		_seenTiles = null;
		_currentIsland = null;
		_frontierTiles = null;
	}
	
	public void DoVoidCheckOn(Tile start, Tile end)
	{
		_seenTiles = new List<Tile>();
		FoundIslands = new List<List<Tile>>();

		DoVoidCheckOnNeighboursOf(start);
		// Debug.Log("--");
		DoVoidCheckOnNeighboursOf(end);

		
		// Debug.Log("-------");
	}

	void DoVoidCheckOnNeighboursOf(Tile t)
	{
		if(t == null)
		{
			return;
		}

		Tile[] tilesToCheck = new Tile[]{
			t.North,
			t.South,
			t.East,
			t.West
		};

		Extensions.Shuffle(tilesToCheck);

		foreach(var tile in tilesToCheck)
		{
			// Debug.Log("Checking "+(tile == null? "null" : tile.name));
			DoVoidCheck(tile);
		}
	} 

	public void PrintResult()
	{
		for(int i = 0; i < FoundIslands.Count; i++)
		{
			var island = FoundIslands[i];

			var contents = "";
			for(int j = 0; j < island.Count; j++)
			{
				if(j > 0)
				{
					contents += ", ";
				}
				
				contents += island[j].name;
			}

			Debug.Log("Island "+i+", length = "+island.Count+": "+contents);
		}
	}

	private void DoVoidCheck(Tile startTile)
	{
		if( !Tile.IsValidTile(startTile) )
		{
			return;
		}

		if(_seenTiles.Contains(startTile))
		{
			//already in an island
			return;
		}

		_currentIsland = new List<Tile>();
		_currentIsland.Add(startTile);

		_frontierTiles = new Queue<Tile>();

		AddCurrentNeighboursToFrontier(startTile);
		while(_frontierTiles.Count > 0)
		{
			var newTile = _frontierTiles.Dequeue();
			if(_currentIsland.Contains(newTile))
			{
				continue;
			}

			_currentIsland.Add(newTile);
			AddCurrentNeighboursToFrontier(newTile);
		}

		FoundIslands.Add(_currentIsland);
		_seenTiles.AddRange(_currentIsland);
	}

	void AddCurrentNeighboursToFrontier(Tile t)
	{
		AddToFrontier(t.North);
		AddToFrontier(t.South);
		AddToFrontier(t.East);
		AddToFrontier(t.West);
	}

	void AddToFrontier(Tile t)
	{
		if(Tile.IsValidTile(t) && !_currentIsland.Contains(t))
		{
			_frontierTiles.Enqueue(t);
		}
	}

	bool AddToIsland(Tile t)
	{
		_currentIsland.Add(t);
		if(_seenTiles.Contains(t))
		{
			return false;
		}

		return true;
	}


}
