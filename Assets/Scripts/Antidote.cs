﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antidote : MonoBehaviour {

	Animator _anim;
	ParticleSystem[] _particles;

	void Awake()
	{
		_anim = GetComponent<Animator>();
		_particles = GetComponentsInChildren<ParticleSystem>();
	}

	void OnTriggerEnter(Collider other)
	{
		GameEngine.Instance.OnAntidoteCollected();
		_anim.SetTrigger("collected");
		StartCoroutine(DissipateParticles());
	}

	IEnumerator DissipateParticles()
	{
		yield return new WaitForSeconds(.2f);

		foreach(var p in _particles)
		{
			var em = p.emission;
			em.enabled = false;
		}

		yield return new WaitForSeconds(1.5f);
		Destroy(gameObject);
	}

	public void Kill()
	{
		_anim.SetTrigger("killed");
		foreach(var p in _particles)
		{
			var em = p.emission;
			em.enabled = false;
		}
	}
}
