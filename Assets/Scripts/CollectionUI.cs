﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionUI : MonoBehaviour {

	public GameObject SliderBarPrefab;
	public GameObject SliderCapPrefab;
	public Transform BarContainer;
	public Image Slider;

	private int _score;
	private int _total;

	public void Init()
	{
		GameEngine.Instance.OnInitLevel.AddListener(SetNumberOfBars);
		GameEngine.Instance.OnGetAntidote.AddListener(HandleGetAntidote);
	}
	
	void SetNumberOfBars()
	{
		_score = 0;
		_total = GameEngine.Instance.AntidotesNeededToWin;
		Slider.fillAmount = 0f;

		foreach(Transform child in BarContainer.transform)
		{
			Destroy(child.gameObject);
		}

		for (int i = 0; i < _total; i++)
		{
			var prefab = i<_total-1? SliderBarPrefab : SliderCapPrefab;
			Instantiate(prefab, BarContainer);
		}
	}

	void HandleGetAntidote()
	{
		_score++;

		var fraction = (float)(_score) / _total;
		//TODO animate me!
		Slider.fillAmount = fraction;
	}
}
