﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : Singleton<LevelManager>
{
	public int CurrentLevel;
	public int LevelCount;

	private bool _canLoadNext;

	public bool EditorLevelSkip = false;

	protected override void SingletonAwake()
	{
		CurrentLevel = 0;
		LevelCount = SceneManager.sceneCountInBuildSettings;
	}

	void Start()
	{
		if(EditorLevelSkip)
		{
			CurrentLevel = LevelCount - 1;
			GameEngine.Instance.DebugCanSpawnEnable();
			DoLevelLoad();
		}
	}
	
	void Update()
	{
		if(!_canLoadNext)
		{
			return;
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			_canLoadNext = false;
			DoLevelLoad();
		}
	}

	public void LoadNextLevel()
	{
		CurrentLevel++;
		if(CurrentLevel < LevelCount)
		{
			_canLoadNext = true;
		}
		else
		{
			TheEnd();
		}
	}

	public void ReloadCurrentLevel()
	{
		_canLoadNext = true;
	}

	private void DoLevelLoad()
	{
		SceneManager.LoadScene(CurrentLevel, LoadSceneMode.Single);
	}

	private void TheEnd()
	{
		GameEngine.Instance.TriggerTheEnd();
	}
}
