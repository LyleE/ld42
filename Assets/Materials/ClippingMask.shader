﻿Shader "LocalMask" {
    Properties {
        _ColorA ("Color A", Color) = (1,1,1,1)
        _ColorB ("Color B", Color) = (0, 0, 0, 1)
      _planePoint ("plane point", Vector) = (0,0,0,0)
      _planeNormal ("plane normal", Vector) = (0,1,0,0)
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
      Cull Off
      CGPROGRAM
      #pragma surface surf Lambert vertex:vert

      struct Input {
          float3 localPos;
      };
      
      float4 _ColorA, _ColorB;
      float4 _planePoint;
      float4 _planeNormal;

        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            o.localPos = v.vertex.xyz;
        }

      void surf (Input IN, inout SurfaceOutput o) {
         if(dot(IN.localPos - (float3)_planePoint,(float3)_planeNormal) > 0)
         {
           o.Albedo = _ColorA;
         }
         else
        {
           o.Albedo = _ColorB;
         }
          //o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
      }
      ENDCG
    }
    Fallback "Diffuse"
  }